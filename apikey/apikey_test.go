package apikey

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"google.golang.org/grpc"

	"bitbucket.org/innius/go-api-context"
	pb "bitbucket.org/innius/go-middleware/apikey/proto"
)

type mockClient struct {
	valid bool
	pb.ApiKeyServiceClient
}

func (m *mockClient) ValidateKey(ctx context.Context, in *pb.ValidateKeyRequest, opts ...grpc.CallOption) (*pb.ValidateKeyReply, error) {
	return &pb.ValidateKeyReply{
		CompanyId: "plantage",
		Id:        "foo",
		Domain:    "bar.com",
		Valid:     m.valid,
		Scopes:    []string{"delete:companies", "delete:persons"},
	}, nil

}

func factory(valid bool) func() (pb.ApiKeyServiceClient, error) {
	return func() (pb.ApiKeyServiceClient, error) {
		return &mockClient{valid: valid}, nil
	}
}

func TestApiKeyAuthenticationMiddleware(t *testing.T) {
	Convey("ApiKey Middleware", t, func() {
		var handlerCtx apicontext.ApiContext

		testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			handlerCtx = apicontext.From(r.Context())
		})

		req := httptest.NewRequest(http.MethodGet, "http://localhost/foo", nil)
		req.Header.Add(apiKeyHeader, "api-key")

		Convey("an valid api key", func() {
			w := httptest.NewRecorder()
			apiKeyAuthentication(factory(true), true)(testHandler).ServeHTTP(w, req)
			Convey("should not return a 401", func() {
				So(w.Code, ShouldEqual, http.StatusOK)
			})
			Convey("should be authenticated", func() {
				So(handlerCtx.Authenticated(), ShouldBeTrue)
			})
			Convey("context should have the company", func() {
				So(handlerCtx.Company(), ShouldNotBeEmpty)
			})

			Convey("context should have the domain", func() {
				So(handlerCtx.Domain(), ShouldNotBeEmpty)
			})

			Convey("context should have the subject", func() {
				So(handlerCtx.Subject(), ShouldNotBeEmpty)
			})
			Convey("context should have the scopes", func() {
				So(handlerCtx.Scopes(), ShouldResemble, []string{"delete:companies", "delete:persons"})
			})
		})
		Convey("an invalid key", func() {
			w := httptest.NewRecorder()
			apiKeyAuthentication(factory(false), true)(testHandler).ServeHTTP(w, req)
			Convey("should return a 401", func() {
				So(w.Code, ShouldEqual, http.StatusUnauthorized)
			})
		})
		Convey("no api key specified", func() {
			req := httptest.NewRequest(http.MethodGet, "http://localhost/foo", nil)
			Convey("api key is optional", func() {
				w := httptest.NewRecorder()
				apiKeyAuthentication(factory(true), false)(testHandler).ServeHTTP(w, req)
				Convey("should return a 200", func() {
					So(w.Code, ShouldEqual, http.StatusOK)
				})
			})
			Convey("api key is mandatory", func() {
				w := httptest.NewRecorder()
				apiKeyAuthentication(factory(true), true)(testHandler).ServeHTTP(w, req)
				Convey("should return a 401", func() {
					So(w.Code, ShouldEqual, http.StatusUnauthorized)
				})

			})
		})
		Convey("api key as url query parameter", func() {
			w := httptest.NewRecorder()
			req := httptest.NewRequest(http.MethodGet, "/foo?x-api-key="+"api-key", nil)
			apiKeyAuthentication(factory(true), true)(testHandler).ServeHTTP(w, req)
			Convey("should return a 200", func() {
				So(w.Code, ShouldEqual, http.StatusOK)
			})
		})
	})
}
