package apikey

import (
	"fmt"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"net/http"

	"bitbucket.org/innius/go-api-context"
	dnsresolver "bitbucket.org/innius/grpc/resolver/dns"
	pb "bitbucket.org/innius/go-middleware/apikey/proto"
	"google.golang.org/grpc"
)

const apiKeyHeader = "x-api-key"

// ApiKeyAuthentication middleware for normal services
// this handler use a single instance pb.ApiKeyServiceClient which uses a dns resolver
func ApiKeyAuthentication(environmentName string, tokenMandatory bool) (func(handler http.Handler) http.Handler, error) {
	client, err := newClient(target(environmentName))
	if err != nil {
		return nil, err
	}
	factory := func() (pb.ApiKeyServiceClient, error) {
		return client, nil
	}
	return apiKeyAuthentication(factory, tokenMandatory), nil
}

// ApiKeyAuthenticationServerless middleware for serverless apis
// Serverless apis do not support background processes which are required for default dns resolver.
// This handler creates a new pb.ApiKeyServiceClient for each request
func ApiKeyAuthenticationServerless(environmentName string, tokenMandatory bool) (func(handler http.Handler) http.Handler, error) {
	factory := func() (pb.ApiKeyServiceClient, error) {
		return newClient(target(environmentName))
	}
	return apiKeyAuthentication(factory, tokenMandatory), nil
}

func target(environmentName string) string {
	return fmt.Sprintf("innius:///apikey-service.%s.private", environmentName)
}

func newClient(target string) (pb.ApiKeyServiceClient, error) {
	conn, err := grpc.Dial(target,
		grpc.WithDefaultServiceConfig(`{"loadBalancingPolicy":"round_robin"}`),
		grpc.WithDisableServiceConfig(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithResolvers(dnsresolver.NewBuilder()),
	)
	if err != nil {
		return nil, err
	}
	return pb.NewApiKeyServiceClient(conn), nil
}

func apiKeyAuthentication(cfn func() (pb.ApiKeyServiceClient, error), tokenMandatory bool) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := apicontext.From(r.Context())
			if ctx.Authenticated() {
				next.ServeHTTP(w, r)
				return
			}
			raw := r.Header.Get(apiKeyHeader)
			if raw == "" {
				raw = r.URL.Query().Get(apiKeyHeader)
			}
			if raw == "" {
				if tokenMandatory {
					http.Error(w, "no api key provided", http.StatusUnauthorized)
					return
				}
				next.ServeHTTP(w, r)
				return
			}
			client, err := cfn()
			if err != nil {
				log.Printf("could not get apikey service client; %+v", err)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}
			resp, err := client.ValidateKey(r.Context(), &pb.ValidateKeyRequest{Apikey: raw})
			if err != nil {
				log.Printf("could not verify apikey; %+v", err)
				http.Error(w, "could not verify the api key", http.StatusInternalServerError)
				return
			}

			if !resp.Valid {
				http.Error(w, "", http.StatusUnauthorized)
				return
			}

			// add key metadata to the request context
			c := apicontext.WithCompanyID(r.Context(), resp.CompanyId)
			c = apicontext.WithDomain(c, resp.Domain)
			c = apicontext.WithSubject(c, resp.Id)
			c = apicontext.WithScopes(c, resp.Scopes)
			c = apicontext.Authenticated(c)
			next.ServeHTTP(w, r.WithContext(c))
		})
	}
}
