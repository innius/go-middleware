package inniusmiddleware

import (
	"fmt"
	"github.com/go-redis/redis"
	"log"
	"net/http"
	"time"
)

//https://redislabs.com/redis-best-practices/basic-rate-limiting/
func RateLimiting(serviceName string, rateLimit int, redisClient redis.Client, resolver func(r *http.Request) string) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			companyId := resolver(r)
			if companyId == "" {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			key := fmt.Sprintf("rate-%s:%s-%v", serviceName, companyId, time.Now().Minute())

			rate, err := redisClient.Get(key).Int()
			if err != nil && err != redis.Nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			if rate > rateLimit {
				w.WriteHeader(http.StatusTooManyRequests)
				return
			}
			pipe := redisClient.TxPipeline()
			pipe.Incr(key)
			pipe.Expire(key, time.Minute)
			if _, err := pipe.Exec(); err != nil {
				log.Printf("An error occured update request rate: %+v", err)
			}
			h.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
