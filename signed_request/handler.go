package sigmiddleware

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/innius/go-api-context"
	"bitbucket.org/innius/httpsig"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/aws/aws-xray-sdk-go/xray"
)

func isSignedRequest(r *http.Request) bool {
	s := r.Header.Get(http.CanonicalHeaderKey("authorization"))
	if s == "" {
		return false
	}
	return strings.HasPrefix(s, "Signature")
}

// SignedRequestAuthentication provides middleware for signed innius requests
// tokenMandatory defines whether or not to continue if no signed request is provided; enable this flag if this
// middleware is used in combination with jwt validation
func SignedRequestAuthentication(kmsapi kmsiface.KMSAPI, tokenMandatory bool) func(handler http.Handler) http.Handler {
	verifier := httpsig.NewVerifier(kmsapi)
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if !isSignedRequest(r) {
				if tokenMandatory {
					http.Error(w, "this endpoint only accepts signed requests", http.StatusBadRequest)
					return
				}
				next.ServeHTTP(w, r)
				return
			}

			// read the context headers from the request headers
			c := apicontext.WithCompanyID(r.Context(), r.Header.Get(httpsig.HttpHeaderCompanyID))
			c = apicontext.WithUserID(c, r.Header.Get(httpsig.HttpHeaderUserID))
			c = apicontext.WithSubject(c, "innius")
			c = apicontext.WithDomain(c, r.Header.Get(httpsig.HttpHeaderDomain))
			xray.AddAnnotation(c, "domain", c.Domain())
			if err := verifier.Verify(r); err != nil {
				xray.AddError(c, err)
				if err, ok := err.(httpsig.VerificationError); ok {
					log.Print(fmt.Sprintf("invalid signature for http request; %+v", err))
					http.Error(w, "", http.StatusUnauthorized)
					return
				}
				log.Print(fmt.Sprintf("error while validating signed http request; %+v", err))
				http.Error(w, "", http.StatusInternalServerError)
				return
			}

			next.ServeHTTP(w, r.WithContext(apicontext.Authenticated(c)))
		})
	}
}
