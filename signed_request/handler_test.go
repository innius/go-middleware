package sigmiddleware_test

import (
	"bitbucket.org/innius/go-api-context"
	"bitbucket.org/innius/go-middleware/signed_request"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"bitbucket.org/innius/httpsig"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func NewRequest() *http.Request {
	req := httptest.NewRequest("POST", "http://example.com/foo",
		strings.NewReader(`{"hello": "world"}`))

	req.Header.Set("Date", "Thu, 05 Jan 2014 21:31:40 GMT")
	req.Header.Set("Content-Type", "application/json")

	return req
}

type kmsMock struct {
	kmsiface.KMSAPI
	plaintext []byte
}

func (m *kmsMock) DecryptWithContext(aws.Context, *kms.DecryptInput, ...request.Option) (*kms.DecryptOutput, error) {
	return &kms.DecryptOutput{
		Plaintext: m.plaintext,
	}, nil
}

var dataKey = &httpsig.DataKey{
	ServiceName:    "the-foo-service",
	Id:             "kms-key-id",
	Plaintext:      []byte("plaintext encryption key"),
	CiphertextBlob: []byte("encrypted data key from kms"),
}

type errorKmsMock struct {
	kmsiface.KMSAPI
}

func (m *errorKmsMock) DecryptWithContext(aws.Context, *kms.DecryptInput, ...request.Option) (*kms.DecryptOutput, error) {
	return nil, errors.Errorf("an aws error occurred")
}

func disableLog() {
	DevNull, _ := os.Create(os.DevNull)
	log.SetOutput(DevNull)
}

func enableLog() {
	log.SetOutput(os.Stdout)
}

func TestHttpRequestAuthenticationMiddleware(t *testing.T) {
	Convey("Http Middleware", t, func() {
		var handlerCtx apicontext.ApiContext

		testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			handlerCtx = apicontext.From(r.Context())
		})

		signer := httpsig.NewRequestSigner(dataKey)
		req := NewRequest()
		companyID, userID, domain := "foo", "bar", "baz"
		ctx := apicontext.WithDomain(apicontext.WithUserID(apicontext.WithCompanyID(req.Context(), companyID), userID), domain)

		req = req.WithContext(ctx)
		So(signer.Sign(req), ShouldBeNil)

		w := httptest.NewRecorder()
		mock := &kmsMock{plaintext: dataKey.Plaintext}

		Reset(func() {
			enableLog()
		})

		Convey("signatures are optional", func() {
			middleware := sigmiddleware.SignedRequestAuthentication(mock, false)
			Convey("a valid signed request", func() {
				middleware(testHandler).ServeHTTP(w, req)
				Convey("should return a 200 OK", func() {
					So(w.Code, ShouldEqual, http.StatusOK)
					So(handlerCtx.Authenticated(), ShouldBeTrue)
					Convey("the api context", func() {
						Convey("has company id", func() {
							So(handlerCtx.Company(), ShouldEqual, companyID)
						})
						Convey("has user id", func() {
							So(handlerCtx.UserID(), ShouldEqual, userID)
						})
						Convey("has domain", func() {
							So(handlerCtx.Domain(), ShouldEqual, domain)
						})
					})
				})
			})
			Convey("an unsigned request", func() {
				middleware(testHandler).ServeHTTP(w, NewRequest())
				Convey("should return a 200 OK", func() {
					So(w.Code, ShouldEqual, http.StatusOK)
					So(handlerCtx.Authenticated(), ShouldBeFalse)
				})
			})
			Convey("an invalid request", func() {
				disableLog()
				tampered := *req
				tampered.Header.Set(httpsig.HttpHeaderServiceName, "bar-service")

				middleware(testHandler).ServeHTTP(w, &tampered)
				Convey("should return a 401 - StatusUnauthorized", func() {
					So(w.Code, ShouldEqual, http.StatusUnauthorized)
				})
			})
		})
		Convey("signatures are mandatory", func() {
			middleware := sigmiddleware.SignedRequestAuthentication(mock, true)
			Convey("a valid signed request", func() {
				middleware(testHandler).ServeHTTP(w, req)
				Convey("should return a 200 OK", func() {
					So(w.Code, ShouldEqual, http.StatusOK)
					So(handlerCtx.Authenticated(), ShouldBeTrue)
				})
			})
			Convey("invalid requests", func() {
				disableLog()
				Convey("an unsigned request", func() {
					middleware(testHandler).ServeHTTP(w, NewRequest())
					Convey("should return a 400 - StatusBadRequest", func() {
						So(w.Code, ShouldEqual, http.StatusBadRequest)
					})
				})
				Convey("a tampered request", func() {
					tampered := *req
					tampered.Header.Set(httpsig.HttpHeaderServiceName, "bar-service")

					middleware(testHandler).ServeHTTP(w, &tampered)
					Convey("should return a 401 - StatusUnauthorized", func() {
						So(w.Code, ShouldEqual, http.StatusUnauthorized)
					})
				})
				Convey("a server error", func() {
					middleware := sigmiddleware.SignedRequestAuthentication(&errorKmsMock{}, true)
					middleware(testHandler).ServeHTTP(w, req)
					Convey("should return a 500 - StatusInternalServerError", func() {
						So(w.Code, ShouldEqual, http.StatusInternalServerError)
					})
				})
			})
		})
	})
}
