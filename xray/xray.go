package xraymiddleware

import (
	"bytes"
	"fmt"
	"github.com/aws/aws-xray-sdk-go/strategy/ctxmissing"
	"github.com/aws/aws-xray-sdk-go/xray"
	"log"
	"net/http"
	"strings"
	"time"
)

func XRayHandler(stackName string, serviceName string) func(http.Handler) http.Handler {
	sn := xray.NewFixedSegmentNamer(serviceName)
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if skip(serviceName, r) {
				next.ServeHTTP(w, r)
				return
			}
			xray.Handler(sn, annotate(stackName, next)).ServeHTTP(w, r)
		})
	}
}

func annotate(stackName string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		xray.AddAnnotation(r.Context(), "stack", stackName)
		next.ServeHTTP(w, r)
	})

}

func skip(serviceName string, r *http.Request) bool {
	if skipPath(serviceName, r) {
		return true
	}
	if skipMethod(r) {
		return true
	}
	return false
}

func skipMethod(r *http.Request) bool {
	return r.Method == http.MethodOptions
}

func skipPath(serviceName string, r *http.Request) bool {
	pathsToSkip := []string{
		"/health",
		fmt.Sprintf("/%s/health", serviceName),
	}

	for _, p := range pathsToSkip {
		if strings.HasPrefix(r.URL.Path, p) {
			return true
		}
	}

	return false
}

func init() {
	client := &http.Client{
		Timeout: 1 * time.Second,
	}

	resp, err := client.Get("http://169.254.169.254/latest/meta-data/local-ipv4")
	if err != nil {
		log.Printf("Could nog get EC2 IP adress: %+v", err)
		return
	}
	defer resp.Body.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	ip := buf.String()

	if err := xray.Configure(xray.Config{
		ContextMissingStrategy: ctxmissing.NewDefaultLogErrorStrategy(),
		DaemonAddr:             fmt.Sprintf("%s:2000", ip),
	}); err != nil {
		log.Printf("Can't initialize xray: %v", err)
	}
}
