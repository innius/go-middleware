package inniusmiddleware

import (
	"net/http"
	"strings"

	openapi "github.com/go-openapi/runtime/middleware"
)

const specFile = "swagger.yml"

// Redoc provides middleware to serve service api specification in a single web page
func Redoc(endpoint string) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodGet && strings.EqualFold(r.URL.Path, endpoint) {
				openapi.Redoc(openapi.RedocOpts{
					Path:    endpoint,
					SpecURL: specFile,
				}, h).ServeHTTP(w, r)
				return
			}
			if r.Method == http.MethodGet && strings.EqualFold(r.URL.Path, "/"+specFile) {
				http.ServeFile(w, r, specFile)
				return
			}

			h.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
