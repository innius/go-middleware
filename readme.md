#go-middleware

middleware for innius apis

![](https://codebuild.us-east-1.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiRVJ0Z3hkUGFKNForMUd6VG9vRldkeVIzeWpIdy9hSGRzRXhuaStCNUloTmJJVzFGS2pFMGY4VW9rdXFFK0JiTGhSUVJWUHFORzBLdzdQSzFSL1liU2NzPSIsIml2UGFyYW1ldGVyU3BlYyI6IlVlMEI1SnVmR0RJeExZZTMiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)

## Redoc 
middleware which exposes an documentation endpoint at the provided path. Service must have a swagger.yml file in its current folder. 

## signed_request 
authentication middleware which verifies signed http requests. Context information from the signed requests are added to the api context of the request. 

### usage
```go
// standalone usage 
SignedRequestAuthentication(kmsapi, true)

// in combination with other authentication middleware
SignedRequestAuthentication(kmsapi, false)

```
