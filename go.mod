module bitbucket.org/innius/go-middleware

require (
	bitbucket.org/innius/go-api-context v1.1.7
	bitbucket.org/innius/grpc v1.2.4
	bitbucket.org/innius/httpsig v1.0.4
	github.com/PuerkitoBio/purell v1.1.0 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/auth0/go-jwt-middleware/v2 v2.0.1
	github.com/aws/aws-sdk-go v1.20.12
	github.com/aws/aws-xray-sdk-go v1.7.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8 // indirect
	github.com/go-chi/cors v1.0.0
	github.com/go-openapi/runtime v0.28.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/golang/protobuf v1.5.3
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/smartystreets/assertions v0.0.0-20190116191733-b6c0e53d7304 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
	github.com/spacemonkeygo/httpsig v0.0.0-20181218213338-2605ae379e47 // indirect
	golang.org/x/net v0.20.0
	google.golang.org/grpc v1.62.1
)

go 1.13
