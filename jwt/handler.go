package jwtmiddleware

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	apicontext "bitbucket.org/innius/go-api-context"
	jwtmiddleware "github.com/auth0/go-jwt-middleware/v2"
	"github.com/auth0/go-jwt-middleware/v2/jwks"
	"github.com/auth0/go-jwt-middleware/v2/validator"
	"github.com/aws/aws-xray-sdk-go/xray"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	"github.com/pkg/errors"
)

type metdata struct {
	CompanyID     string `json:"companyid"`
	CompanyStatus int    `json:"company_status"`
	UserID        string `json:"userid"`
	Domain        string `json:"domain"`
	Scope         string `json:"scope"`
}

// this is how we get them from the service
type claims struct {
	Audience     []string `json:"aud"`
	ExpiratesAt  float64  `json:"exp,omitempty"`
	IssuedAt     float64  `json:"iat"`
	Issuer       string   `json:"iss"`
	Subject      string   `json:"sub"`
	UserMetadata metdata  `json:"https://innius.com"`
	Scope        string   `json:"scope"`
	KeyType      string   `json:"key_type,omitempty"`
}

func (c claims) Validate(ctx context.Context) error {
	return nil
}

const issuer, audience, jwksEndpoint = "issuer", "audience", "jwks_endpoint"

func getParametersByPath(ssmapi ssmiface.SSMAPI, path string, params map[string]string, nextToken *string) error {
	p, err := ssmapi.GetParametersByPath(&ssm.GetParametersByPathInput{
		Path:      aws.String(path),
		Recursive: aws.Bool(false),
	})

	if err != nil {
		return errors.Wrap(err, "could not read auth0 parameters from ssm")
	}

	for i := range p.Parameters {
		param := p.Parameters[i]
		name := aws.StringValue(param.Name)[len(path)+1:]
		params[name] = aws.StringValue(param.Value)
	}

	if p.NextToken != nil {
		return getParametersByPath(ssmapi, path, params, p.NextToken)
	}
	return nil
}

func NewFromEnv() (func(handler http.Handler) http.Handler, error) {
	opts := Options{
		Issuer:   os.Getenv("AUTH_JWT_AUTH0_ISSUER_URL"),
		Audience: os.Getenv("AUTH_JWT_AUTH0_AUDIENCE"),
	}
	if opts.Audience == "" {
		return nil, errors.New("Audience must be specified")
	}
	if opts.Issuer == "" {
		return nil, errors.New("Issuer must be specified")
	}

	v, err := newValidator(opts)
	if err != nil {
		return nil, err
	}
	return tokenAuthenticationWithOptions(v), nil
}

// factory which returns a new instance of token authentication middleware
// auth0 settings are read from ssm parameter store in the specified innius stack
func New(ssmapi ssmiface.SSMAPI, stackName string) (func(handler http.Handler) http.Handler, error) {
	path := fmt.Sprintf("/%s/auth0", stackName)
	params := map[string]string{}
	if err := getParametersByPath(ssmapi, path, params, nil); err != nil {
		return nil, err
	}
	opts := Options{
		Issuer:   params[issuer],
		Audience: params[audience],
	}

	if opts.Audience == "" {
		return nil, errors.New("Audience must be specified")
	}
	if opts.Issuer == "" {
		return nil, errors.New("Issuer must be specified")
	}

	v, err := newValidator(opts)
	if err != nil {
		return nil, err
	}
	return tokenAuthenticationWithOptions(v), nil
}

// Options provides options for TokenAuthentication middleware
type Options struct {
	// the audience of the token, for example "https://test.innius.com",
	Audience string
	// the issuer of the token, for example "https://auth.test.innius.com/",
	Issuer string
}

func newValidator(opts Options) (*validator.Validator, error) {
	issuerURL, err := url.Parse(opts.Issuer)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse the issuer url: %v", opts.Issuer)
	}

	provider := jwks.NewCachingProvider(issuerURL, 5*time.Minute)

	// Set up the validator.
	jwtValidator, err := validator.New(
		provider.KeyFunc,
		validator.RS256,
		issuerURL.String(),
		[]string{opts.Audience},
		validator.WithCustomClaims(func() validator.CustomClaims {
			return &claims{}
		}),
	)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to set up the validator")
	}

	return jwtValidator, nil
}

func tokenAuthenticationWithOptions(v *validator.Validator) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := apicontext.From(r.Context())
			if ctx.Authenticated() {
				next.ServeHTTP(w, r)
				return
			}
			mw := jwtmiddleware.New(v.ValidateToken, jwtmiddleware.WithCredentialsOptional(false))
			mw.CheckJWT(authenticatedContext(next)).ServeHTTP(w, r)
		})
	}
}

func authenticatedContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := apicontext.From(r.Context())
		token := r.Context().Value(jwtmiddleware.ContextKey{}).(*validator.ValidatedClaims)
		claims := token.CustomClaims.(*claims)
		ctx = apicontext.WithCompanyID(ctx, claims.UserMetadata.CompanyID)
		ctx = apicontext.WithCompanyStatus(ctx, claims.UserMetadata.CompanyStatus)
		ctx = apicontext.WithUserID(ctx, claims.UserMetadata.UserID)
		ctx = apicontext.WithDomain(ctx, claims.UserMetadata.Domain)
		ctx = apicontext.WithSubject(ctx, claims.UserMetadata.UserID)
		ctx = apicontext.WithScopes(ctx, strings.Split(claims.UserMetadata.Scope, " "))
		ctx = apicontext.Authenticated(ctx)

		xray.AddAnnotation(r.Context(), "domain", ctx.Domain())

		next.ServeHTTP(w, r.WithContext(ctx))
	})

}
