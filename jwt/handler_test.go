package jwtmiddleware

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	apicontext "bitbucket.org/innius/go-api-context"
	"github.com/auth0/go-jwt-middleware/v2/validator"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	"github.com/dgrijalva/jwt-go"
	. "github.com/smartystreets/goconvey/convey"
)

type ssmMock struct {
	parameters []*ssm.Parameter
	ssmiface.SSMAPI
}

func newSSMMock(issuer, audience, endpoint string) *ssmMock {
	parameters := make([]*ssm.Parameter, 0)
	path := "/chair/auth0/"
	if issuer != "" {
		parameters = append(parameters, &ssm.Parameter{Name: aws.String(path + issuer), Value: aws.String(issuer)})
	}
	if audience != "" {
		parameters = append(parameters, &ssm.Parameter{Name: aws.String(path + audience), Value: aws.String(audience)})
	}
	return &ssmMock{
		parameters: parameters,
	}

}

func (m *ssmMock) GetParametersByPath(input *ssm.GetParametersByPathInput) (*ssm.GetParametersByPathOutput, error) {
	return &ssm.GetParametersByPathOutput{
		Parameters: m.parameters,
	}, nil
}

func NewRequest(t string) *http.Request {
	r := httptest.NewRequest(http.MethodGet, "/query", nil)

	r.Header.Add("Authorization", "Bearer "+t)

	return r
}

func TestJWTMiddleware(t *testing.T) {
	var handlerCtx apicontext.ApiContext

	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerCtx = apicontext.From(r.Context())
	})
	// note: we use a different signature algorithm (RS256) in our deployed system. This is an asymmetric algorithm.
	// In this test case we use HS256 which is a symmetric algorithm because it is easier to create a token with a simple
	// private key.
	Convey("token middleware", t, func() {
		key := []byte("supersecret")
		v, err := validator.New(
			func(ctx context.Context) (interface{}, error) {
				return key, nil
			}, validator.HS256, issuer, []string{audience}, validator.WithCustomClaims(func() validator.CustomClaims {
				return &claims{}
			}))
		if err != nil {
			t.Fatal(err)
		}
		m := tokenAuthenticationWithOptions(v)

		middleware := m(testHandler)
		Convey("a request without a jwt", func() {
			r := httptest.NewRequest(http.MethodGet, "/foo", nil)
			Convey("should return a 400", func() {
				w := httptest.NewRecorder()
				middleware.ServeHTTP(w, r)
				So(w.Code, ShouldEqual, http.StatusBadRequest)
			})
			Convey("already authenticated by other middleware", func() {
				c := apicontext.Authenticated(context.Background())
				w := httptest.NewRecorder()
				middleware.ServeHTTP(w, r.WithContext(c))
				Convey("token should not be validated", func() {
					So(w.Code, ShouldEqual, http.StatusOK)
				})
			})
		})
		Convey("a request with a valid token", func() {
			m := metdata{
				CompanyID:     "innius",
				CompanyStatus: 3,
				UserID:        "john",
				Domain:        "innius.com",
				Scope:         "delete:companies delete:persons",
			}
			token, err := generateTestSignedString(key, m)
			if err != nil {
				t.Fatal(err)
			}

			req := NewRequest(token)
			w := httptest.NewRecorder()
			middleware.ServeHTTP(w, req)
			Convey("Should return 200", func() {
				So(w.Code, ShouldEqual, http.StatusOK)
			})
			Convey("I should be authenticated", func() {
				So(handlerCtx.Authenticated(), ShouldBeTrue)
			})
			Convey("api context should have my company id", func() {
				So(handlerCtx.Company(), ShouldEqual, m.CompanyID)
			})
			Convey("api context should have my company status", func() {
				So(handlerCtx.CompanyStatus(), ShouldEqual, m.CompanyStatus)
			})
			Convey("api context should have my user id", func() {
				So(handlerCtx.UserID(), ShouldEqual, m.UserID)
			})
			Convey("api context should have my domain", func() {
				So(handlerCtx.Domain(), ShouldEqual, m.Domain)
			})
			Convey("api context should have my scopes", func() {
				So(handlerCtx.Scopes(), ShouldResemble, []string{"delete:companies", "delete:persons"})
			})
		})
	})
}

func generateTestSignedString(mySigningKey []byte, m metdata) (string, error) {
	// Create the Claims
	claims := jwt.MapClaims{
		"nbf":                time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
		"iss":                issuer,
		"aud":                []string{audience},
		"exp":                time.Now().Add(time.Hour).Unix(),
		"https://innius.com": m,
		"scope":              "super-admin",
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(mySigningKey)
}
