#TokenAuthentication

jwt authentication middleware for innius apis

Configuration is read from ssm parameter store. Add the following parameters to ssm parameter store:
``` 
* /stack/audience = "https://test.innius.com" 
* /stack/issuer= "https://auth.test.innius.com/",
* /stack/jwks_endpoint= "https://auth.test.innius.com/.well-known/jwks.json
```

## usage 
```
// create a new middleware instance
mw, err := jwtmiddleware.New(ssmClient, "chair")

// use middleware in routes
r.Use(mw).Get(....)

```